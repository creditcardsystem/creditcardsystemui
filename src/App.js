import './App.css';
import React, { Component }  from 'react';
import Appbar from './component/Appbar'
import CardDetails from './component/CardDetails'

function App() {
  return (
    <div className="App">
        <Appbar/>
        <CardDetails/>
    </div>
  );
}

export default App;
