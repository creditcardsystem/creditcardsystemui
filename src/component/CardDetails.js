import * as React from 'react';
import {useState} from 'react'
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Container from '@mui/material/Container';
import Paper from '@mui/material/Paper';
import Button from '@mui/material/Button';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';

export default function CardDetails() {
    const paperStyle={padding:'10px 10px', width:300,margin:"10px auto"}
    const[name,setName]=useState('')
    const[cardNumber,setCardNumber]=useState('')
    const[cardLimit,setCardLimit]=useState('')
    const[cards,setCards]=useState([])
    const[error,setError]=useState(null)
   

    const handleClick=(e)=>{
      e.preventDefault()
      const card ={name,cardNumber,cardLimit}
      console.log(card)
      fetch("http://localhost:8081/creditcardsystem/v1/card",{
        method:"POST",
        headers:{"Content-Type":"application/json"},
        body:JSON.stringify(card)
  
    }).then(res=>{
      console.log(res);
    if(!res.ok){
     throw Error("Invalid Card");
    } 
  return res.json();})
    .then((result)=>{
      console.log(result);
      setCards(result.card_detail_list);
    }
  ).catch(err => {
    console.log(err.message)
  setError(err.message);
  })
  }
  

  return (
    
    <Container>
    <Paper elevation={2} style={paperStyle}>
            <h2 style={{color:"black"} }>Add</h2>
           {error && <div style={{color:"red"} }>{error}</div> }
   <Box component="form" sx={{'& > :not(style)': { m: 1 },}} noValidate autoComplete="off">
      <TextField  id="outlined-basic" label="Name" variant="outlined" defaultValue="Name" value={name} onChange={(e)=>setName(e.target.value)}/><br/>
      <TextField  id="outlined-basic" label="Card Number" variant="outlined" defaultValue="1111-2222-3333-4444" value={cardNumber} onChange={(e)=>setCardNumber(e.target.value)} /><br/>
      <TextField  id="outlined-basic" label="Limit" variant="outlined" defaultValue="0.0" value={cardLimit} onChange={(e)=>setCardLimit(e.target.value)}/><br/>
      <Button variant="contained" size="small" onClick={handleClick}>Add</Button>
    </Box>
    </Paper>
    <h2>Existing Cards</h2>
  
<TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Name</TableCell>
            <TableCell align="right">Card Number</TableCell>
            <TableCell align="right">Balance</TableCell>
            <TableCell align="right">Limit</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>

          {cards.map((card) => (
            <TableRow
              key={card.id}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell align="right">{card.name}</TableCell>
              <TableCell align="right">{card.cardNumber}</TableCell>
              <TableCell align="right">£ {card.balance}</TableCell>
              <TableCell align="right">£ {card.cardLimit}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
 
    </Container>
  );
}
